package com.jquoridors.core;

import com.jquoridors.core.action.*;

import java.util.List;

public class Game implements Runnable {

    private List<Player> players;
    private WorldModel worldModel;
    private ActionValidator actionValidator;
    private ActionExecutor actionExecutor;
    private Player winner;

    @Override
    public void run() {
        players.forEach(Runnable::run);

        do {

                players.forEach(player ->
                {
                    try {
                        perform(player, player.act(worldModel));
                        if (hasEnded()) {
                            winner = player;

                            //Should be handled!
                            System.exit(0);
                        }
                    } catch (ActionIsInvalidException
                            | ActionExecutionException ex) {
                        System.err.println(ex.getMessage());
                    }
                });

        } while(true);

    }

    private void perform(Player player, Action action)
            throws ActionIsInvalidException,
                    ActionExecutionException {
        //validate action
        if (!actionValidator.isValid(player, action)) {
            throw new ActionIsInvalidException(player, action);
        }

        //perform action on board
        actionExecutor.execute(player, action);
    }

    private Boolean hasEnded() {
        return true;
    }
}
