package com.jquoridors.core;

import com.jquoridors.core.action.Action;

public interface Player extends Runnable {

    Action act(WorldModel worldModel);
}
