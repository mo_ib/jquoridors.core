package com.jquoridors.core;


import com.jquoridors.core.board.Board;

public interface WorldModel {

    Board getBoard();

}
