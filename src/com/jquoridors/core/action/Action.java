package com.jquoridors.core.action;

import com.jquoridors.core.board.Position;

public interface Action {

    Position getPosition();
}
