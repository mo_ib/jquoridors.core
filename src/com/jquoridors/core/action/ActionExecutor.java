package com.jquoridors.core.action;

import com.jquoridors.core.Player;

public interface ActionExecutor {

    void execute(Player player, Action action)
            throws ActionExecutionException;
}
