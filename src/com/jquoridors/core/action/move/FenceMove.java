package com.jquoridors.core.action.move;

import com.jquoridors.core.board.FenceDirection;
import com.jquoridors.core.board.Position;

public class FenceMove extends AbstractMove {

    private FenceDirection direction;

    public FenceMove(Position position, FenceDirection direction) {
        super(position);
        this.direction = direction;
    }

    public FenceDirection getDirection() {
        return direction;
    }
}
