package com.jquoridors.core.action.move;

import com.jquoridors.core.board.Position;

public class PawnMove extends AbstractMove {

    private Position currentPosition;

    public PawnMove(Position position) {
        this(position, null);
    }

    public PawnMove(Position position, Position currentPosition) {
        super(position);
        this.currentPosition = currentPosition;
    }

    public Position getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(Position currentPosition) {
        this.currentPosition = currentPosition;
    }
}
