package com.jquoridors.core.board;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class Board {

    private final Map<BoardItemType, TreeSet<BoardItem>> items =
            new HashMap<>();

    public void init() {
        TreeSet<BoardItem> cells = new TreeSet<>();
        for (Integer cellCounter = 1; cellCounter <= 81; ++cellCounter) {
            cells.add(new Cell());
        }
        items.put(BoardItemType.CELL, cells);
    }
}
