package com.jquoridors.core.board;

public class Position {

    Character col;
    Integer row;

    public Position(Character col, Integer row) {
        this.col = col;
        this.row = row;
    }

    public Character getCol() {
        return col;
    }

    public void setCol(Character col) {
        this.col = col;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }
}
